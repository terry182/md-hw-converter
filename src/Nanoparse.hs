module Nanoparse (
        module Nanoparse,
        module Control.Applicative
        )where

import Data.Char
import Control.Monad
import Control.Applicative

newtype Parser a = Parser (String -> [(a, String)])

parse :: Parser a -> (String -> [(a, String)])
parse (Parser p) = p 


instance Functor Parser where
    fmap f (Parser cs) = Parser (\s -> [(f a,b) | (a, b) <- cs s])

instance Applicative Parser where
    pure = return
    (Parser cs1) <*> (Parser cs2) = Parser (\s -> [(f a, s2) | (f, s1) <- cs1 s, (a, s2) <- cs2 s1])

instance Monad Parser where
    return a = Parser (\cs -> [(a, cs)])
    p >>= f = Parser (\cs -> concat [parse (f a) cs' | (a, cs') <- parse p cs])

instance MonadPlus Parser where 
    mzero = Parser (\cs -> [])
    mplus p q = Parser (\cs -> parse p cs ++ parse q cs)

instance Alternative Parser where
    empty = mzero
    (<|>) = option

bind :: Parser a -> (a -> Parser b) -> Parser b
bind p f = Parser (\cs -> concat [parse (f a) cs' | (a, cs') <- parse p cs])

item :: Parser Char
item = Parser (\cs -> case cs of
                         []   -> []
                         (c:cs) -> [(c, cs)])

-- Apply two parser to same input, concat the result.
combine :: Parser a -> Parser a -> Parser a
combine p q = Parser (\inp -> parse p inp ++ parse q inp)

-- Combine two optional path 
option :: Parser a -> Parser a -> Parser a
option p q = Parser $ 
    \inp -> case parse p inp of 
                [] -> parse q inp
                res -> res 
                
-- Checking whether the current character matches a given predicate
satisfy :: (Char -> Bool) -> Parser Char
satisfy p = item `bind` \c -> 
    if p c
    then return c
    else mzero

-- Check if one of them satisfies
oneOf :: [Char] -> Parser Char
oneOf s = satisfy (flip elem s)

-- parses one or more occurrences of p, separated by op and returns a value obtained by a recursing until failure
-- op is operator with left-assocociate
chainl :: Parser a -> Parser (a -> a -> a) -> a -> Parser a
chainl p op a = (p `chainl1` op) <|> return a   

-- parses non-empty sequences of items separated by operators that associate to the left
chainl1 :: Parser a -> Parser (a -> a -> a) -> Parser a
p `chainl1` op = do { a <- p; rest a }
    where rest a = (do f <- op
                       b <- p
                       rest (f a b))
                   <|> return a

char :: Char -> Parser Char
char c = satisfy (c ==)

string :: String -> Parser String
string [] = return []
string (c:cs) = do { char c; string cs; return (c:cs) }

-- Read a token defined by Parser p, and eliminate spaces
token :: Parser a -> Parser a
token p = do {spaces; a <- p; spaces; return a}

-- Read specific string and remove spaces
reserved :: String -> Parser String
reserved s = token (string s)

spaces :: Parser String
spaces = many $ oneOf " \n\r"

digit :: Parser Char
digit = satisfy isDigit

natural :: Parser Integer
natural = read <$> some digit

parens :: Parser a -> Parser a
parens m = do
    reserved "("
    n <- m   
    reserved ")"
    return n

number :: Parser Int
number = do 
    s <- string "-" <|> return []
    cs <- some digit
    return $ read (s ++ cs)

manyTill :: Parser a -> Parser end -> Parser [a]
manyTill a end = scan
    where scan = (end >> return []) <|> liftM2 (:) a scan

runParser :: Parser a -> String -> a
runParser m s = 
    case parse m s of
        [(res, [])] -> res
        [(_, rs)]   -> error "Parser did not consume entire stream"
        _           -> error "Parser error."
