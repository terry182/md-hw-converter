-- Markdown Language
-- [] Italic * string *,
-- [] Bold ** string **
-- [] Bold Italic *** string ***
-- [x] Escape \
-- [x] Math $ string $ 

import Nanoparse 
import Data.Char

data Text = Bold [Text]
          | Italic [Text]
          | Math String
          | Lit String
          deriving Show

--- Seperated by non-word characters
-- word :: Parser String

-- NormalText Except Seperator
normChar :: Parser Char
normChar = satisfy (`notElem` "$*#\\")

-- Single normal text
norm :: Parser Text
norm = do 
    n <- some (escape <|> normChar)
    return (Lit n)

-- Parse escape charaters.
escape :: Parser Char
escape  = do
    reserved "\\"
    item

-- Parenthesis 
-- Try to avoid alias by doing top down approach | failed
-- Reworkkkkk
-- Math
math :: Parser Text
math = do
    reserved "$"
    n <- manyTill (escape <|> item) (char '$')
    return (Math n)

bold m = do
    reserved "**"
    n <- manyTill (escape <|> item) (string "**")
    return (Bold (runParser m n))

italic :: Parser [Text] -> Parser Text
italic m = do
    reserved "*"
    n <- m
    reserved "*"
    return (Italic n)

-- Parse error shit workaround.
-- Don't want to modify runParser function
anyShit = do
    n <- some item
    return (Lit n)

-- Parse single block of text
text :: Parser Text
text = norm <|> math <|> bold (some text) <|> italic (some text) <|> anyShit

-- Main program
run :: String -> [Text]
run =  runParser $ many text

main :: IO()
main = do
    n <- getLine 
    print $ run n 
