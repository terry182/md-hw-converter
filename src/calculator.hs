import Nanoparse

-------------------------------------------------------------------------------
-- Calulator parser
-------------------------------------------------------------------------------

-- number = [ "-" ] digit { digit }.
-- digit = "0" | "1" | ... | "8" | "9".
-- expr = term { addop term }.
-- term = factor { mulop factor }.
-- factor = "(" expr ")" | number.
-- addop = "+" | "-".
-- mulop = "*".
--
data Expr = Add Expr Expr
          | Mul Expr Expr
          | Sub Expr Expr
          | Lit Int
          deriving Show

eval :: Expr -> Int
eval ex = case ex of
    Add a b -> eval a + eval b
    Mul a b -> eval a * eval b
    Sub a b -> eval a - eval b
    Lit n -> n

int :: Parser Expr
int = do
    n <- (token number)
    return (Lit n)

-- Parser infix operator x, with return value of a function
infixOp :: String -> (a -> a -> a) -> Parser (a -> a -> a)
infixOp x f = reserved x >> return f 

addop :: Parser (Expr -> Expr -> Expr)
addop = (infixOp "+" Add) <|> (infixOp "-" Sub)

mulop :: Parser (Expr -> Expr -> Expr)
mulop = infixOp "*" Mul

factor :: Parser Expr
factor = int <|> parens expr 

term :: Parser Expr
term = factor `chainl1` mulop

expr :: Parser Expr
expr = term `chainl1` addop

run :: String -> Expr
run = runParser expr

main :: IO()
main = getLine >>= print . eval . run
